using Android.Graphics;
using System;
using System.IO;

namespace CBQA.Exam.Common
{
    public static class ConverterHelper
    {
        public static byte[] ImageToByteArray(Bitmap bitmap)
        {
            var stream = new MemoryStream();
            bitmap.Compress(Bitmap.CompressFormat.Png, 0, stream);
            return stream.ToArray();
        }

        public static string ConvertToBase64(Bitmap bitmap)
        {
            return Convert.ToBase64String(ImageToByteArray(bitmap));            
        }
    }
}