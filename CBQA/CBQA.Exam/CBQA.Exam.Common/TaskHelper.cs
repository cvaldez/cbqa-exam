using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Threading;

namespace CBQA.Exam.Common
{
    public static class TaskHelper
    {
        public static Task<T> WaitAsync<T>(this Task<T> task)
        {
            var wait = new ManualResetEventSlim(false);

            var continuation = task.ContinueWith(_ =>
            {
                wait.Set();
                return _.Result;
            });
            wait.Wait();
            return continuation;
        }
    }
}