using System;
using System.Text.RegularExpressions;

namespace CBQA.Exam.Common
{
    public static class ValidatorHelper
    {
        private const string EMAIL_REG_EXPRESSION = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
            + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
            + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";


        static Regex ValidEmailRegex = CreateValidEmailRegex();

        private static Regex CreateValidEmailRegex()
        {
            return new Regex(EMAIL_REG_EXPRESSION, RegexOptions.IgnoreCase);
        }

        public static bool EmailIsValid(string emailAddress)
        {
            return ValidEmailRegex.IsMatch(emailAddress);
        }

        public static bool PasswordsAreEqual(string pass, string repass)
        {
            return pass.Equals(repass);
        }



        public static bool CheckURLValid(this string source)
        {
            Uri uriResult;
            return Uri.TryCreate(source, UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp;
        }
        public static bool IsAnURL(string url)
        {
            return CheckURLValid(url);
        }
    }
}