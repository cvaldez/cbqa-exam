using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CBQA.Exam.Models;

namespace CBQA.Exam.Droid.ViewModels
{
    public class MovieVM : Movie
    {
        public override string ToString()
        {
            return Name + " (" + YearOfRelease+")";
        }
    }
}