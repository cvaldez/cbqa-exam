using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;
using CBQA.Exam.Common;
using CBQA.Exam.Dal.Internal;
using CBQA.Exam.Droid.Fragments;
using CBQA.Exam.Models;

namespace CBQA.Exam.Droid.Activities
{
    [Activity(
        Label = "@string/app_name",
        MainLauncher = false,
        LaunchMode = LaunchMode.SingleTop,
        Icon = "@drawable/Icon",
        NoHistory = false        
        )]
    public class MainActivity : BaseActivity
    {

        DrawerLayout drawerLayout;
        NavigationView navigationView;
        TextView lblUsername;
        protected override int LayoutResource  {  get { return Resource.Layout.activity_main; }  }
        

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            lblUsername = FindViewById<TextView>(Resource.Id.lblDrawerUserName);
            lblUsername.Text = BaseConstants.BASIC_WELCOME_MESSAGE + GetCurrentUser().FullName;


            navigationView.NavigationItemSelected += (sender, e) =>
            {
                e.MenuItem.SetChecked(true);
                switch (e.MenuItem.ItemId)
                {
                    case Resource.Id.nav_home_1:
                        ListItemClicked(0);
                        break;
                    case Resource.Id.nav_home_3:
                        Snackbar.Make(drawerLayout, BaseConstants.BASIC_CONFIRMATION_LOGOUT, Snackbar.LengthLong).SetAction(BaseConstants.YES_WORD, (v) => { DestroySession(); }).Show();
                        break;
                }
                drawerLayout.CloseDrawers();
            };


            if (savedInstanceState == null)
            {
                ListItemClicked(0);
            }
        }

        int oldPosition = -1;


        private void ListItemClicked(int position)
        {
            if (position == oldPosition)
                return;

            oldPosition = position;

            Android.Support.V4.App.Fragment fragment = null;
            switch (position)
            {
                case 0:
                    fragment = FragmentMain.NewInstance();
                    break;
                case 2:
                    break;
            }

            SupportFragmentManager.BeginTransaction()
                .Replace(Resource.Id.content_frame, fragment)
                .Commit();
        }


        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }
        

        private void DestroySession()
        {
            var db = new DatabaseManager();
            if(db.LogoutInDB())
                StartActivity(typeof(LoginActivity));
        }


        private User GetCurrentUser()
        {
            var db = new DatabaseManager();
            return db.GetUser();
        }




    }
}

