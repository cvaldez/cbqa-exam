using Android.App;
using Android.OS;
using Android.Widget;
using CBQA.Exam.Common;
using CBQA.Exam.Dal.External;
using CBQA.Exam.Models;
using System.Threading.Tasks;

namespace CBQA.Exam.Droid.Activities
{
    [Activity(Label = "Register User")]
    public class RegisterUserActivity : Activity
    {
        EditText txtFullName, txtPass, txtRepass, txtEmail;
        Button btnSave;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_registeruser);

            txtFullName = FindViewById<EditText>(Resource.Id.txtRegUsrFullName);
            txtEmail = FindViewById<EditText>(Resource.Id.txtRegUsrMail);
            txtPass = FindViewById<EditText>(Resource.Id.txtRegUsrPass);
            txtRepass = FindViewById<EditText>(Resource.Id.txtRegUsrRePass);
            btnSave = FindViewById<Button>(Resource.Id.btnRegUsrSignin);

            btnSave.Click += async delegate {
                var user = new User()
                {
                    Email = txtEmail.Text,
                    FullName = txtFullName.Text,
                    Password = HashHelper.MD5(txtPass.Text),
                    Id = 0,
                    Status = true
                };
                await SaveNewUser(user);              
            };
        }


        public async Task SaveNewUser(User obj)
        {
            if (!ValidatorHelper.EmailIsValid(txtEmail.Text))
            {
                LocalNotificationHelper.ShowSimpleToast(this, "The provided Email does not seems like an email");
            }
            else
            {
                if (!ValidatorHelper.PasswordsAreEqual(txtPass.Text, txtRepass.Text))
                    LocalNotificationHelper.ShowSimpleToast(this, "The password fields do not match");
                else
                {
                    var userClient = new UserClient(this);
                    await userClient.RegisterNew(obj);
                    base.OnBackPressed();
                    OverridePendingTransition(Resource.Animation.fab_in, Resource.Animation.fab_out);
                }
            }
           

        }
    }
}