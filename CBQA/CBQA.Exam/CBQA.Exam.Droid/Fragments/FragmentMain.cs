using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;
using CBQA.Exam.Dal.External;
using CBQA.Exam.Droid.Activities;
using CBQA.Exam.Droid.Adapters;
using CBQA.Exam.Models;
//using Clans.Fab;
using System.Collections.Generic;

namespace CBQA.Exam.Droid.Fragments
{
    public class FragmentMain : Fragment
    {
        FloatingActionButton floatingButtonMovies;
        ListView listView;
        TextView lblThereIsNoElements;
        View alt_view;
        MoviesAdapter adapter;
        SwipeRefreshLayout refresher;
        Context contextMain;

        public List<Movie> listMovies { get; set; } = new List<Movie>();

        public FragmentMain()
        {
            RetainInstance = true;
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
        }
        public async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            contextMain = Activity;
            
            //GUI CONTROLS 
            var mc = new MovieClient(Activity);
            listMovies = await mc.GetAll();
            adapter = new MoviesAdapter(listMovies, Activity);

            listView = alt_view.FindViewById<ListView>(Resource.Id.MoviesListView);
            listView.Adapter = adapter;
            lblThereIsNoElements = alt_view.FindViewById<TextView>(Resource.Id.lblThereIsNoElements);
            floatingButtonMovies = alt_view.FindViewById<FloatingActionButton>(Resource.Id.floatingButton);
            refresher = alt_view.FindViewById<SwipeRefreshLayout>(Resource.Id.swipe1);
            refresher.SetColorScheme(Android.Resource.Color.HoloOrangeDark);

            if (listMovies.Count == 0)
                lblThereIsNoElements.Visibility = ViewStates.Visible;
            else if (listMovies.Count > 0)
                lblThereIsNoElements.Visibility = ViewStates.Invisible;




            refresher.Refresh += (sender, e) =>
            {
                RefreshListView();
                refresher.DestroyDrawingCache();
                refresher.Refreshing = false;

            };
            
            floatingButtonMovies.Click += delegate
            {
                LaunchNewMovie();
            };

        }



        public override void OnResume()
        {
            base.OnResume();
        }

        public static FragmentMain NewInstance()
        {

            return new FragmentMain { Arguments = new Bundle() };
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            alt_view = inflater.Inflate(Resource.Layout.fragment_main, null);
            return alt_view;
        }

        private void LaunchNewMovie()
        {
            StartActivity(new Intent(Activity, typeof(RegisterMovieActivity)));
        }




        public async void RefreshListView()
        {
            listView.Adapter = null;
            var mc = new MovieClient(contextMain);
            listMovies = await mc.GetAll(false);
            adapter = new MoviesAdapter(listMovies, contextMain);
            listView.Adapter = adapter;


            floatingButtonMovies.Click += delegate
            {
                LaunchNewMovie();
            };

        }

    }


}