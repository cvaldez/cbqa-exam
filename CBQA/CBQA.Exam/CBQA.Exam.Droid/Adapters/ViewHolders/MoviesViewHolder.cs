using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CBQA.Exam.Models;

namespace CBQA.Exam.Droid.Adapters.ViewHolders
{
    class MoviesViewHolder : Java.Lang.Object
    {

        public TextView Title { get; set; }

        public ImageView Image { get; set; }

        public TextView YearOfPublish { get; set; }
    }
}