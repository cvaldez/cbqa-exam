using Android.Content;
using Android.Graphics;
using CBQA.Exam.Common;
using Imgur.API;
using Imgur.API.Authentication.Impl;
using Imgur.API.Endpoints.Impl;
using Imgur.API.Enums;
using Imgur.API.Models;
using Imgur.API.Models.Impl;
using System;
using System.Threading.Tasks;

namespace CBQA.Exam.Dal.External
{
    public class ImgurApiClient
    {

        public OAuth2Token CreateToken()
        {
            return new OAuth2Token(
                BaseConstants.IMGUR_ACCESSTOKEN,
                BaseConstants.IMGUR_REFRESHTOKEN,
                BaseConstants.IMGUR_TOKENTYPE,
                BaseConstants.IMGUR_ACCOUNTID,
                BaseConstants.IMGUR_ACCOUNTUSERNAME,
                int.Parse(BaseConstants.IMGUR_EXPIRESIN)
                );
        }
        
        public Task<IOAuth2Token> RefreshMyToken()
        {
            var client = new ImgurClient(BaseConstants.IMGUR_CLIENT_ID, BaseConstants.IMGUR_SECRET_KEY);
            var oauth = new OAuth2Endpoint(client);
            var token = oauth.GetTokenByRefreshTokenAsync(BaseConstants.IMGUR_REFRESHTOKEN);
            return token;
        }

        public async Task UploadToImgur(Bitmap bmp, Context ctx)
        {
            try
            {
                var tokenNew = await RefreshMyToken();
                var ld =LocalNotificationHelper.ShowSimpleProgressDialog(ctx, BaseConstants.BASIC_UPLOADING_IMG_MESSAGE);
                ld.Show();
                var imgurClient = new ImgurClient(BaseConstants.IMGUR_CLIENT_ID, BaseConstants.IMGUR_SECRET_KEY, CreateToken());
                var endpoint = new ImageEndpoint(imgurClient);
                IImage image;
                var imageConverted = ConverterHelper.ImageToByteArray(bmp);
                image = await endpoint.UploadImageBinaryAsync(imageConverted, BaseConstants.IMGUR_ALBUMID, "title_" + (Guid.NewGuid().ToString().Substring(1, 8)), "cbqa-rel_" + (Guid.NewGuid().ToString().Substring(1, 10)));
                ld.Hide();
            }
            catch (Exception imgurEx)
            {
                throw imgurEx;
            }
        }


        

    }

}
