﻿using Android.Content;
using CBQA.Exam.Common;
using CBQA.Exam.Models;
using Firebase;
using Firebase.Storage;
using Firebase.Xamarin.Database;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CBQA.Exam.Dal.External
{
    public class MovieClient
    {
        Context _ctx;
        FirebaseClient _firebase;
        string _baseEntity;
        
        public MovieClient(Context context)
        {
            _ctx = context;
            _firebase = new FirebaseClient(BaseConstants.FIREBASE_DB_URL);
            _baseEntity = BaseConstants.FIREBASE_ENTITY_MOVIES;
        }

        
        public async Task<List<Movie>> GetAll(bool ShowLoading = true)
        {
            if(ShowLoading)
            {
                var pd = LocalNotificationHelper.ShowSimpleProgressDialog(_ctx, BaseConstants.BASIC_SAVING_MESSAGE);
                pd.Show();
                var myFirebaseMovies = await GetFromFirebase();
                pd.Hide();
                return myFirebaseMovies;
            }
            else
                return await GetFromFirebase();
        }

        public async Task<List<Movie>> GetFromFirebase()
        {
            var lst = new List<Movie>();
            var items = await _firebase.Child(_baseEntity).OnceAsync<Movie>();
            foreach (var item in items)
            {
                lst.Add(item.Object);
            }

            return lst;
        }
        
        public async Task RegisterNew(Movie obj)
        {
            var pd = LocalNotificationHelper.ShowSimpleProgressDialog(_ctx, BaseConstants.BASIC_SAVING_MESSAGE);
            pd.Show();
            var item = await _firebase.Child(_baseEntity).PostAsync(obj);
            pd.Hide();
            LocalNotificationHelper.ShowSimpleToast(_ctx, BaseConstants.MOVIE_SAVED_OK);
        }

        public async Task Update(Movie obj)
        {
            var pd = LocalNotificationHelper.ShowSimpleProgressDialog(_ctx, BaseConstants.BASIC_LOGIN_MESSAGE);
            pd.Show();
            await _firebase.Child(_baseEntity).PutAsync(obj);
            pd.Hide();
        }

        public async Task Delete(Movie obj)
        {
            var pd = LocalNotificationHelper.ShowSimpleProgressDialog(_ctx, BaseConstants.BASIC_LOGIN_MESSAGE);
            pd.Show();
            await _firebase.Child(_baseEntity).DeleteAsync();
            pd.Hide();
        }
        
        public async Task<string> UploadImage(byte[] image)
        {
            var pd = LocalNotificationHelper.ShowSimpleProgressDialog(_ctx, BaseConstants.BASIC_UPLOADING_IMG_MESSAGE);
            pd.Show();
            var task = new FirebaseStorage(BaseConstants.FIREBASE_STORAGE_URL)
                .Child("cbqamovies")
                .PutAsync(new MemoryStream(image));
            pd.Hide();
            return await task;
        }

    }
}
