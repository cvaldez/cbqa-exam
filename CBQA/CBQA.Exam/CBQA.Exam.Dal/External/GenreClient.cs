using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CBQA.Exam.Dal.Models;
using CBQA.Exam.Common;
using System.Threading.Tasks;
using Firebase.Xamarin.Database;

namespace CBQA.Exam.Dal.External
{
    public class GenreClient
    {
        Context _ctx;
        FirebaseClient _firebase;
        string _baseEntity;


        public GenreClient(Context context)
        {
            _ctx = context;
            _firebase = new FirebaseClient(BaseConstants.FIREBASE_DB_URL);
            _baseEntity = BaseConstants.FIREBASE_ENTITY_GENRES;
            
        }



        private static List<Genre> GetGenres()
        {
            return new List<Genre>
            {
                new Genre { Id = 1, Name="Action", Detail=null},
                new Genre { Id = 2, Name="Adventure", Detail=null},
                new Genre { Id = 3, Name="Drama", Detail=null},
                new Genre { Id = 4, Name="Commedy", Detail=null},
                new Genre { Id = 5, Name="Sci-Fi", Detail=null},
                new Genre { Id = 6, Name="Historical", Detail=null},
                new Genre { Id = 7, Name="Musical", Detail=null},
                new Genre { Id = 8, Name="War", Detail=null},
                new Genre { Id = 9, Name="Western", Detail=null},
                new Genre { Id = 10, Name="Animation", Detail=null}
            };
        }

        public async void UploadGenres()
        {
            foreach (var item in GetGenres())
            {
                await RegisterNew(item);
            }
        }




        public async Task<List<Genre>> GetAll()
        {
            var pd = LocalNotificationHelper.ShowSimpleProgressDialog(_ctx, BaseConstants.BASIC_LOGIN_MESSAGE);
            pd.Show();
            var lst = new List<Genre>();
            var items = await _firebase.Child(_baseEntity).OnceAsync<Genre>();
            foreach (var item in items)
            {
                lst.Add(item.Object);
            }
            pd.Hide();
            return lst;
        }


        public async Task RegisterNew(Genre obj)
        {
            var pd = LocalNotificationHelper.ShowSimpleProgressDialog(_ctx, BaseConstants.BASIC_SAVING_MESSAGE);
            pd.Show();
            var item = await _firebase.Child(_baseEntity).PostAsync(obj);
            pd.Hide();
            LocalNotificationHelper.ShowSimpleToast(_ctx, BaseConstants.MOVIE_SAVED_OK);
        }
    }
}