﻿using System.Collections.Generic;
using CBQA.Exam.Models;
using SQLite;

namespace CBQA.Exam.Dal.Internal
{
    public class RepositoryMovies : IRepository<Movie>
    {
        public void Add(Movie obj)
        {
            using (var dbcon = new SQLiteConnection(DatabaseManager.FullDatabasePath))
            {
                dbcon.Insert(obj);
            }
        }

        public bool Delete(int id)
        {
            using (var dbcon = new SQLiteConnection(DatabaseManager.FullDatabasePath))
            {
                var obj = dbcon.Table<Movie>().Where(m => m.Id == id).FirstOrDefault();
                dbcon.Delete(obj);
                return true;
            }
        }

        public IEnumerable<Movie> GetAll()
        {
            using (var dbcon = new SQLiteConnection(DatabaseManager.FullDatabasePath))
            {
                return dbcon.Table<Movie>().Where(m => m.Status == true);
            }
        }


        public Movie Get()
        {
            using (var dbcon = new SQLiteConnection(DatabaseManager.FullDatabasePath))
            {
                return dbcon.Table<Movie>().Where(m => m.Status == true).FirstOrDefault();
            }
        }
        public Movie GetById(int id)
        {
            using (var dbcon = new SQLiteConnection(DatabaseManager.FullDatabasePath))
            {
                return dbcon.Table<Movie>().Where(m => m.Status == true && m.Id == id).FirstOrDefault();
            }
        }

        public bool Update(Movie obj)
        {
            using (var dbcon = new SQLiteConnection(DatabaseManager.FullDatabasePath))
            {
                dbcon.Update(obj);
                return true;
            }
        }
    }
}
