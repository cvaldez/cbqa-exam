﻿using CBQA.Exam.Models;

namespace CBQA.Exam.Dal.Internal
{
    public interface IRepositoryMovies : IRepository<Movie>
    {

    }
}
