﻿using CBQA.Exam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBQA.Exam.Dal.Internal
{
    public interface IRepositoryUsers : IRepository<User>
    {
    }
}
