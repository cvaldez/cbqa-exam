﻿using System.Collections.Generic;

namespace CBQA.Exam.Dal.Internal
{
    public interface IRepository<GenericEntity> where GenericEntity : class
    {
        IEnumerable<GenericEntity> GetAll();
        GenericEntity Get();
        //virtual GenericEntity GetById(int id);
        void Add(GenericEntity obj);
        bool Update(GenericEntity obj);
        bool Delete(int id);
    }
}
