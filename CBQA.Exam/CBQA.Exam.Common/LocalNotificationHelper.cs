using Android.App;
using Android.Content;
using Android.Widget;
using System;

namespace CBQA.Exam.Common
{
    public class LocalNotificationHelper
    {
        public static void ShowSimpleToast(Context ctx, string msg)
        {
            Toast.MakeText(ctx, msg, ToastLength.Long).Show();
        }

        public static ProgressDialog ShowSimpleProgressDialog(Context ctx, string msg)
        {
            var pd = new ProgressDialog(ctx);
            msg = BaseConstants.BASIC_LOADING_MESSAGE;
            pd.SetMessage(msg);
            return pd;
        }
        
        public static void ShowSimpleAlertDialog(Context ctx, string msg, Action positive, Action negative, string title)
        {
            title = string.Empty;
            var alert = new AlertDialog.Builder(ctx);
            alert.SetTitle(title);
            alert.SetMessage(msg);
            alert.SetPositiveButton(BaseConstants.YES_WORD, (senderAlert, args) => {
                positive();
            });
            alert.SetNegativeButton(BaseConstants.NO_WORD, (senderAlert, args) => {
                negative();
            });
            var dialog = alert.Create();
            dialog.Show();
        }

        
    }
}