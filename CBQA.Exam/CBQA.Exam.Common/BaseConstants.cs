namespace CBQA.Exam.Common
{
    public static class BaseConstants
    {
        public const string FIREBASE_DB_URL = "https://cbqaxamarintest.firebaseio.com/";
        public const string FIREBASE_STORAGE_URL = "cbqaxamarintest.appspot.com";
        public const string FIREBASE_ENTITY_USERS = "users";
        public const string FIREBASE_ENTITY_MOVIES = "movies";
        public const string FIREBASE_ENTITY_STORE_POSTERS = "cbqa_movies";
        public const string FIREBASE_FOLDER_STORE_POSTERS = "posters";
        public const string FIREBASE_IMAGE_FORMAT = ".png";


        #region IMGUR_CREDENTIALS
        public const string 
            IMGUR_CLIENT_ID ="29ecd18fe84ea52",
            IMGUR_SECRET_KEY ="5383fc1f9c458798ec1abbe1fd3632bbf016a50d",
            IMGUR_ACCESSTOKEN= "5af3d8dc44a8023dfd1b429ec90f57d5bc5f0764",
            IMGUR_REFRESHTOKEN= "7f756f4b3e52b46aa7405cf5f74bda9aa20a3ffb",
            IMGUR_TOKENTYPE="bearer",
            IMGUR_ACCOUNTID= "54217073",
            IMGUR_ACCOUNTUSERNAME= "charlyvldz",
            IMGUR_EXPIRESIN= "315360000",
            IMGUR_ALBUMID= "ViRwY"
            ;


        public const string DEFAULT_IMAGE_URL = "https://yt3.ggpht.com/--DEvaE3sHBA/AAAAAAAAAAI/AAAAAAAAAAA/enpq6xPVlmI/s900-c-k-no-mo-rj-c0xffffff/photo.jpg";
        #endregion

        private static string IMGUR_URL_CALLBACK = "https://app.getpostman.com/oauth2/callback%20#access_token=5af3d8dc44a8023dfd1b429ec90f57d5bc5f0764&expires_in=315360000&token_type=bearer&refresh_token=7f756f4b3e52b46aa7405cf5f74bda9aa20a3ffb&account_username=charlyvldz&account_id=54217073";
       



        public const string BASIC_LOADING_MESSAGE = "Loading...";
        public const string BASIC_LOGIN_MESSAGE = "Authenticating...";
        public const string BASIC_SAVING_MESSAGE = "Saving...";
        public const string BASIC_ERROR_CREDENTIALS = "Error in Email or Password";
        public const string BASIC_ERROR_ISNOTEMAIL = "For login, you must capture a valid email";
        public const string BASIC_ERROR_EMPTYCREDENTIALS = "The Email and Password fields should not be empty";
        public const string BASIC_CONFIRMATION_LOGOUT = "Are you sure that you want to logout?";

        public const string YES_WORD = "Yes";
        public const string NO_WORD = "No";

        public const string USER_SAVED_OK = "User stored successfully";
        public const string MOVIE_SAVED_OK = "Movie stored successfully";
        public const string BASIC_WELCOME_MESSAGE = "Welcome back ";

        public static string BASIC_UPLOADING_IMG_MESSAGE = "Uploading image...";
        public static string FIREBASE_ENTITY_GENRES = "genres";
    }
}