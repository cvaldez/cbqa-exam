using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using System.Net;
using Android.Graphics.Drawables;

namespace CBQA.Exam.Common
{
    public static class ImageHelper
    {
        public static Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;
            using (var webClient = new WebClient())
            {
                if (url == string.Empty || !ValidatorHelper.IsAnURL(url))
                    url = BaseConstants.DEFAULT_IMAGE_URL;

                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
            }

            return imageBitmap;
        }


        static Dictionary<string, Drawable> cache = new Dictionary<string, Drawable>();

        public static Drawable Get(Context context, string url)
        {
            if (!cache.ContainsKey(url))
            {
                var drawable = Drawable.CreateFromStream(context.Assets.Open(url), null);
                cache.Add(url, drawable);
            }
            return cache[url];
        }


        public static Bitmap GetBitmapFromImageView(ImageView imageView)
        {
            imageView.BuildDrawingCache(true);
            var bitmap = imageView.GetDrawingCache(true);
            var drawable = (BitmapDrawable)imageView.Drawable;
            var bitmap2 = drawable.Bitmap;
            return bitmap2;
        }
    }
}