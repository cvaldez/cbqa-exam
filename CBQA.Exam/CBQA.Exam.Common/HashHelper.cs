using System;
using System.Text;

namespace CBQA.Exam.Common
{
    public static class HashHelper
    {
        public static string MD5(string stringToHash)
        {
            var md5 = System.Security.Cryptography.MD5.Create();
            var encoding = new ASCIIEncoding();
            byte[] stream = null;
            var sb = new StringBuilder();
            stream = md5.ComputeHash(encoding.GetBytes(stringToHash));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }
        public static string Token()
        {
            long i = 1;
            foreach (byte b in Guid.NewGuid().ToByteArray()) i *= ((int)b + 1);
            return MD5(string.Format("{0:x}", i - DateTime.Now.Ticks));
        }
        public static string Base64Encode(string stringToHash)
        {
            byte[] byt = Encoding.UTF8.GetBytes(stringToHash);
            return Convert.ToBase64String(byt);
        }
        public static string Base64Decode(string stringToHash)
        {
            byte[] b = Convert.FromBase64String(stringToHash);
            return Encoding.UTF8.GetString(b);
        }
        public static string SHA1(string stringToHash)
        {
            var sha1 = System.Security.Cryptography.SHA1.Create();
            var encoding = new ASCIIEncoding();
            byte[] stream = null;
            var sb = new StringBuilder();
            stream = sha1.ComputeHash(encoding.GetBytes(stringToHash));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }
        public static string SHA256(string stringToHash)
        {
            var sha256 = System.Security.Cryptography.SHA256.Create();
            var encoding = new ASCIIEncoding();
            byte[] stream = null;
            var sb = new StringBuilder();
            stream = sha256.ComputeHash(encoding.GetBytes(stringToHash));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }
        public static string SHA384(string stringToHash)
        {
            var sha384 = System.Security.Cryptography.SHA384.Create();
            var encoding = new ASCIIEncoding();
            byte[] stream = null;
            var sb = new StringBuilder();
            stream = sha384.ComputeHash(encoding.GetBytes(stringToHash));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }
        public static string SHA512(string stringToHash)
        {
            var sha512 = System.Security.Cryptography.SHA512.Create();
            var encoding = new ASCIIEncoding();
            byte[] stream = null;
            var sb = new StringBuilder();
            stream = sha512.ComputeHash(encoding.GetBytes(stringToHash));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }
    }
}