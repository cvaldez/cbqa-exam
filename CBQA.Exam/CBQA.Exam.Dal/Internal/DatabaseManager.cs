﻿using CBQA.Exam.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CBQA.Exam.Dal.Internal
{
    public class DatabaseManager
    {
        public static string DBLocation = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        public static string DBName = "AppCBQA.db3";
        public static string FullDatabasePath = string.Empty;

        #region ExceptionMessages
        private const string DatabaseErrorMessage = "Error Building the Database";

        #endregion



        public DatabaseManager()
        {
            FullDatabasePath = Path.Combine(DBLocation, DBName);
            repoUsers = new RepositoryUsers();
            CreateDatabaseObjects();
        }



        public bool CreateDatabaseObjects()
        {

            try
            {
                using (var dbcon = new SQLiteConnection(FullDatabasePath))
                {
                    dbcon.CreateTable<User>();
                    dbcon.CreateTable<Movie>();
                    return true;
                }

            }
            catch (Exception)
            {
                throw new Exception(DatabaseErrorMessage);
            }
        }


        #region Users_Crud
        RepositoryUsers repoUsers;

        public bool AddUser(User obj)
        {
            repoUsers.Add(obj);
            return true;
        }

        public User GetUser()
        {
            return repoUsers.Get();
        }


        public bool UpdateUser(User obj)
        {
            return repoUsers.Update(obj);
        }

        public IEnumerable<User> GetAllUsers()
        {
            return repoUsers.GetAll();
        }

        public bool LogoutInDB()
        {
            return repoUsers.TruncateTables();
        }


        public bool IsAnyoneHere()
        {
            return repoUsers.IsThereUsers();
        }
        #endregion



    }
}
