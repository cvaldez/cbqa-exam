namespace CBQA.Exam.Dal.Internal
{
    public enum DatabaseType
    {
        None,
        Files,
        Sqlite,
        CouchDb,
        SharedPreferences
    }
}