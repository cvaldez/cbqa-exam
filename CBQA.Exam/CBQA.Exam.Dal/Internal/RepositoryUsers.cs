﻿using System.Collections.Generic;
using CBQA.Exam.Models;
using SQLite;
using Java.Lang;
using System.IO;

namespace CBQA.Exam.Dal.Internal
{
    public class RepositoryUsers : IRepository<User>
    {
        public RepositoryUsers()
        {
            DatabaseManager.FullDatabasePath = Path.Combine(DatabaseManager.DBLocation, DatabaseManager.DBName);
        }

        public void Add(User obj)
        {
            try
            {
                using (var dbcon = new SQLiteConnection(DatabaseManager.FullDatabasePath))
                {
                    dbcon.Insert(obj);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            using (var dbcon = new SQLiteConnection(DatabaseManager.FullDatabasePath))
            {
                var obj = dbcon.Table<User>().Where(m => m.Id == id).FirstOrDefault();
                dbcon.Delete(obj);
                return true;
            }
        }

        public IEnumerable<User> GetAll()
        {
            using (var dbcon = new SQLiteConnection(DatabaseManager.FullDatabasePath))
            {
                return dbcon.Table<User>().Where(m => m.Status == true);
            }
        }

        public User Get()
        {
            using (var dbcon = new SQLiteConnection(DatabaseManager.FullDatabasePath))
            {
                return dbcon.Table<User>().Where(m => m.Status == true).FirstOrDefault();
            }
        }

        public bool Update(User obj)
        {
            using (var dbcon = new SQLiteConnection(DatabaseManager.FullDatabasePath))
            {
                dbcon.Update(obj);
                return true;
            }
        }


        public bool IsThereUsers()
        {
            using (var dbcon = new SQLiteConnection(DatabaseManager.FullDatabasePath))
            {
                var i = dbcon.Table<User>().Count();
                return i > 0 ? true : false;
            }
        }

        public bool TruncateTables()
        {
            using (var dbcon = new SQLiteConnection(DatabaseManager.FullDatabasePath))
            {
                dbcon.DeleteAll<User>();
                dbcon.DeleteAll<Movie>();
                return true;
            }
        }



    }
}
