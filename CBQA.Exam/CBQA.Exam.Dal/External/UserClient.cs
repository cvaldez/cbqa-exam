﻿using Android.Content;
using CBQA.Exam.Common;
using CBQA.Exam.Models;
using Firebase.Xamarin.Database;
using System.Threading.Tasks;

namespace CBQA.Exam.Dal.External
{
    public class UserClient
    {
        Context _ctx;
        FirebaseClient _firebase;
        string _baseEntity;


        public UserClient(Context context)
        {
            _ctx = context;
            _firebase = new FirebaseClient(BaseConstants.FIREBASE_DB_URL);
            _baseEntity = BaseConstants.FIREBASE_ENTITY_USERS;
        }


        public async Task<User> Login(User obj)
        {
            var pd = LocalNotificationHelper.ShowSimpleProgressDialog(_ctx, BaseConstants.BASIC_LOGIN_MESSAGE);
            pd.Show();
            User usrToLogin = null;
            var items = await _firebase.Child(_baseEntity).OnceAsync<User>();
            foreach (var item in items)
            {
                if (item.Object.Email == obj.Email)
                    if (item.Object.Password == obj.Password)
                        usrToLogin = item.Object;
                    else
                        usrToLogin = null;
            }
            pd.Hide();
            return usrToLogin;
        }

        public async Task RegisterNew(User obj)
        {
            var pd = LocalNotificationHelper.ShowSimpleProgressDialog(_ctx, BaseConstants.BASIC_SAVING_MESSAGE);
            pd.Show();
            var item = await _firebase.Child(_baseEntity).PostAsync(obj);
            pd.Hide();
            LocalNotificationHelper.ShowSimpleToast(_ctx, BaseConstants.USER_SAVED_OK);
        }

        public async Task Update(User obj)
        {
            var pd = LocalNotificationHelper.ShowSimpleProgressDialog(_ctx, BaseConstants.BASIC_LOGIN_MESSAGE);
            pd.Show();
            await _firebase.Child(_baseEntity).PutAsync(obj);
            pd.Hide();
        }


    }
}
