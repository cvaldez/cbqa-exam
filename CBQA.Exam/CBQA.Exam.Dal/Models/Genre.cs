namespace CBQA.Exam.Dal.Models
{
    public class Genre
    {
        public byte Id { get; set; }
        public string Name { get; set; }
        public string Detail { get; set; }

       
    }
}