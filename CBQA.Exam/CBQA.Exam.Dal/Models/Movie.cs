﻿
using System;

namespace CBQA.Exam.Models
{
    public class Movie
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int YearOfRelease { get; set; }
        public DateTime DateOfAdd { get; set; }
        public string PosterURL { get; set; }
        public string Description { get; set; }
        public string Genre { get; set; }
        public bool Status { get; set; }
    }
}
