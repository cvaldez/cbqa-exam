using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Content;
using Android.Provider;
using Android.Runtime;
using CBQA.Exam.Dal.External;
using CBQA.Exam.Models;
using CBQA.Exam.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CBQA.Exam.Droid.Activities
{
    [Activity(Label = "Register Movie")]
    public class RegisterMovieActivity : BaseActivity
    {

        EditText txtNameOfMovie, txtYearOfRelease, txtUrlPhotoPoster, txtdescription;
        ImageButton btnTakePhoto;
        Button btnSave, btnDelete;
        ImageView imgMovie;
        Spinner spnGenres;
        List<string> genres;
        string genreSelected = string.Empty;
        string urlPoster = string.Empty;

        public Android.Graphics.Bitmap ImageOfMovie { get; set; }
        
        protected override int LayoutResource { get { return Resource.Layout.activity_registermovie; } }

        protected async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(LayoutResource);

            txtNameOfMovie = FindViewById<EditText>(Resource.Id.txtRegMovName);
            txtYearOfRelease = FindViewById<EditText>(Resource.Id.txtRegMovYearOfRelease);
            txtUrlPhotoPoster = FindViewById<EditText>(Resource.Id.txtRegMovUrlPosterMovie);
            txtdescription = FindViewById<EditText>(Resource.Id.txtRegMovDescription);
            btnTakePhoto = FindViewById<ImageButton>(Resource.Id.btnRegMovTakePosterPhoto);
            btnSave = FindViewById<Button>(Resource.Id.btnRegMovSave);
            btnDelete = FindViewById<Button>(Resource.Id.btnRegMovDelete);
            imgMovie = FindViewById<ImageView>(Resource.Id.imgRegMovMovie);
            spnGenres = FindViewById<Spinner>(Resource.Id.spnGenres);

            btnDelete.Visibility = ViewStates.Invisible;
            await GetGenres();

            var adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerItem, genres);
            spnGenres.Adapter = adapter;


            Toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            if (Toolbar != null)
            {
                SetSupportActionBar(Toolbar);
                SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                SupportActionBar.SetHomeButtonEnabled(true);
            }


            btnTakePhoto.Click += delegate
            {
                TakePhoto();
            };

            btnSave.Click += delegate
            {
                SaveAll();
            };

            btnDelete.Click += delegate
            {

            };

            spnGenres.ItemSelected += SpnGenres_ItemSelected;

        }

        private void SpnGenres_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            genreSelected = spnGenres.SelectedItem.ToString();
        }

        private async void Delete()
        {
            var date = DateTime.Now;
            var desc = txtdescription.Text;
            var urlposter = txtUrlPhotoPoster.Text;
            var id = 0;
            var name = txtNameOfMovie.Text;
            var status = true;
            var year = 2010;
            var movie = new Movie
            {
                DateOfAdd = date,
                Description = desc,
                PosterURL = urlposter,
                Id = id,
                Name = name,
                Status = status,
                YearOfRelease = year
            };
            var mc = new MovieClient(this);
            await mc.Delete(movie);
        }

        private async void SaveAll()
        {
            var date = DateTime.Now;
            var desc = txtdescription.Text;
            var id = 0;
            var name = txtNameOfMovie.Text;
            var status = true;
            var yr = txtYearOfRelease.Text.ToString();
            var year = (txtYearOfRelease.Text != string.Empty) ? Convert.ToInt32(yr) : 2017;
            var mc = new MovieClient(this);
            var bitmap = ImageHelper.GetBitmapFromImageView(imgMovie);
            var imgByteArr = ConverterHelper.ImageToByteArray(bitmap);
            await mc.UploadImage(imgByteArr);
            var urlposter = txtUrlPhotoPoster.Text;

            var movie = new Movie
            {
                DateOfAdd = date,
                Description = desc,
                PosterURL = urlposter,
                Id = id,
                Name = name,
                Status = status,
                YearOfRelease = year,
                Genre = genreSelected
            };
            await mc.RegisterNew(movie);
        }

        private void TakePhoto()
        {
            var intent = new Intent(MediaStore.ActionImageCapture);
            StartActivityForResult(intent, 0);
        }

        private async Task GetGenres()
        {
            var mc = new GenreClient(this);
            var genresDB = await mc.GetAll();
            genres = new List<string>();
            foreach (var item in genresDB)
            {
                genres.Add(item.Name);
            }
        }

        protected async override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            ImageOfMovie = (Android.Graphics.Bitmap)data.Extras.Get("data");
            imgMovie.SetImageBitmap(ImageOfMovie);
            var mc = new MovieClient(this);
            var bitmap = ImageHelper.GetBitmapFromImageView(imgMovie);
            var imgByteArr = ConverterHelper.ImageToByteArray(bitmap);            
            txtUrlPhotoPoster.Text = await mc.UploadImage(imgByteArr);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            base.OnBackPressed();
            return base.OnOptionsItemSelected(item);
        }








    }
}
