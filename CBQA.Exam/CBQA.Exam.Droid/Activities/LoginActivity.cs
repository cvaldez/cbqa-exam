using Android.App;
using Android.OS;
using Android.Widget;
using CBQA.Exam.Common;
using CBQA.Exam.Dal.External;
using CBQA.Exam.Dal.Internal;
using CBQA.Exam.Models;
using System.Threading.Tasks;

namespace CBQA.Exam.Droid.Activities
{
    [Activity(
        Label = "@string/app_name", 
        MainLauncher = true,
        NoHistory = false
        )]
    public class LoginActivity : Activity
    {
        EditText txtLoginUsername, txtLoginPassword;
        Button btnLogin, btnLoginRegister;
        ProgressBar circularLoadingBar;


        protected async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_login);

            btnLogin = FindViewById<Button>(Resource.Id.btnLoginLogin);
            btnLoginRegister = FindViewById<Button>(Resource.Id.btnLoginNewUser);
            txtLoginUsername = FindViewById<EditText>(Resource.Id.txtLoginUser);
            txtLoginPassword = FindViewById<EditText>(Resource.Id.txtLoginPassword);
            circularLoadingBar = FindViewById<ProgressBar>(Resource.Id.loadingBar);


            btnLogin.Click += async delegate
            {
                await Login();
            };

            btnLoginRegister.Click += delegate
            {
                LaunchRegister();
            };


            if(IsThereAnybodyLoggedIn())
                StartActivity(typeof(MainActivity));


        }


        private bool IsThereAnybodyLoggedIn()
        {
            var db = new DatabaseManager();
            if (db.IsAnyoneHere())
                return true;
            else
            {
                db.LogoutInDB();
                return false;
            }
        }



        private void LaunchRegister()
        {
            StartActivity(typeof(RegisterUserActivity));
        }


        private bool SaveLoginInternally(User obj)
        {
            var db = new DatabaseManager();
            return db.AddUser(obj);
        }

        private async Task Login()
        {
            var emailUser = txtLoginUsername.Text;
            var pass = HashHelper.MD5(txtLoginPassword.Text);
            if (ValidatorHelper.EmailIsValid(emailUser))
            {
                var usr = new User
                {
                    Email = emailUser,
                    Password = pass
                };
                if (usr.Email != string.Empty && txtLoginPassword.Text != string.Empty)
                {
                    var userClient = new UserClient(this);
                    var login = await userClient.Login(usr);
                    if (login != null)
                    {
                        SaveLoginInternally(login);
                        StartActivity(typeof(MainActivity));
                    }
                    else
                        LocalNotificationHelper.ShowSimpleToast(this, BaseConstants.BASIC_ERROR_CREDENTIALS);
                }
                else
                    LocalNotificationHelper.ShowSimpleToast(this, BaseConstants.BASIC_ERROR_EMPTYCREDENTIALS);
            }
            else
                LocalNotificationHelper.ShowSimpleToast(this, BaseConstants.BASIC_ERROR_ISNOTEMAIL);

        }
    }
}