using System.Collections.Generic;
using Android.Content;
using Android.Views;
using Android.Widget;
using CBQA.Exam.Models;
using CBQA.Exam.Common;
using CBQA.Exam.Dal.External;

namespace CBQA.Exam.Droid.Adapters
{
    public class MoviesAdapter : BaseAdapter<Movie>
    {
        Context _context;
        List<Movie> listOfMovies;


        #region CONSTRUCTORS
        public MoviesAdapter(Context context)
        {
            _context = context;
        }

        public MoviesAdapter(List<Movie> movies, Context ctx)
        {
            listOfMovies = movies;
            _context = ctx;
        }
        #endregion



        public override Movie this[int position] { get { return listOfMovies[position]; } }
        public override int Count { get { return listOfMovies.Count; } }


        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var v = convertView;
            if (v == null)
            {
                v = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.item_movie, parent, false);
                var photoMovie = v.FindViewById<ImageView>(Resource.Id.photoImageView);
                var name = v.FindViewById<TextView>(Resource.Id.nameTextView);
                var year = v.FindViewById<TextView>(Resource.Id.departmentTextView);
                var layout = v.FindViewById<RelativeLayout>(Resource.Id.item_layout);
                v.Tag = new MoviesViewHolder
                {
                    Title = name,
                    Image = photoMovie,
                    YearOfPublish = year
                };

                v.LongClick += delegate
                {
                    LocalNotificationHelper.ShowSimpleAlertDialog(_context, "�Are you sure, it will delete everything?", Delete, null, "");                    
                };

            }

            var holder = (MoviesViewHolder)v.Tag;
            var imagefromInternet = ImageHelper.GetImageBitmapFromUrl(listOfMovies[position].PosterURL);
            holder.Image.SetImageBitmap(imagefromInternet);
            holder.Title.Text = listOfMovies[position].Name;
            holder.YearOfPublish.Text = listOfMovies[position].YearOfRelease.ToString();
            return v;
        }

        public async void Delete()
        {
            var mc = new MovieClient(_context);
            await mc.Delete(new Movie());
        }

        public void NotDelete()
        {

        }
    }

    class MoviesViewHolder : Java.Lang.Object
    {

        public TextView Title { get; set; }

        public ImageView Image { get; set; }

        public TextView YearOfPublish { get; set; }
    }


}